import './App.css';
import DrawingCanvas from './components/DrawingCanvas';
import axios from 'axios';
import background from './images/background-orange-shapes.svg';
import { useEffect, useState } from 'react';
import loadingImage from './images/loading-image.svg';

function App() {

  const [canvas, setCanvas] = useState(null);

  const submitDrawing = async (pixelsList) => {
    const response = await axios.post('http://localhost:5000/predict', {
      pixelsList: pixelsList
    });
    // const response = await axios.get('http://localhost:5000');

    return response;
  };

  useEffect(() => {
    setTimeout(() => setCanvas(<DrawingCanvas submitDrawing={submitDrawing} />), 1000);
  }, [])

  return (
    // state < 1 ?
      // <div className="App">
      //   <img className="LoadingImage" src={loadingImage} alt="Loading image" />
      // </div>
    // :
    <div className="App">
      <img src={background} className="App-background"></img>
      <header className="App-header"></header>
      {canvas === null ?
        <img className="App-loadingImage LoadingImage" src={loadingImage} alt="Loading image" />
      :
        canvas}
      <footer className="App-footer">
        <div style={{ color: 'darkgrey' }}>Draw: click and hold + move mouse</div>
        <div style={{ color: 'grey' }}>Paint area: double click</div>
        <div style={{ color: 'darkgrey' }}>Undo: control + z</div>
        <div style={{ color: 'grey' }}>Redo: control + y</div>
      </footer>
    </div>
  );
}

export default App;

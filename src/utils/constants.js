const constants = {
    DRAW_AREA_WIDTH: 38.64,
    DRAW_AREA_HEIGHT: 38.64,
    MEASURE_TYPE: 'vh',
    PIXEL_COUNT_X: 128,
    PIXEL_COUNT_Y: 128,
    PEN_SIZE: 1,
    COLORS: {
        COLOR_WHITE: 'rgb(255, 255, 255)',
        COLOR_YELLOW: 'rgb(255, 255, 0)',
        COLOR_ORANGE: 'rgb(255, 102, 0)',
        COLOR_RED: 'rgb(221, 0, 0)',
        COLOR_MAGENTA: 'rgb(255, 0, 153)',
        COLOR_PURPLE: 'rgb(51, 0, 153)',
        COLOR_BLUE: 'rgb(0, 0, 204)',
        COLOR_CYAN: 'rgb(0, 153, 255)',
        COLOR_GREEN: 'rgb(0, 170, 0)',
        COLOR_DARK_GREEN: 'rgb(0, 102, 0)',
        COLOR_BROWN: 'rgb(102, 51, 0)',
        COLOR_TAN: 'rgb(153, 102, 51)',
        COLOR_LIGHT_GREY: 'rgb(187, 187, 187)',
        COLOR_MEDIUM_GREY: 'rgb(136, 136, 136)',
        COLOR_DARK_GREY: 'rgb(68, 68, 68)',
        COLOR_BLACK: 'rgb(0, 0, 0)'
    },
    COLORS_ARRAY: [
        'rgb(255, 255, 255)',
        'rgb(255, 255, 0)',
        'rgb(255, 102, 0)',
        'rgb(221, 0, 0)',
        'rgb(255, 0, 153)',
        'rgb(51, 0, 153)',
        'rgb(0, 0, 204)',
        'rgb(0, 153, 255)',
        'rgb(0, 170, 0)',
        'rgb(0, 102, 0)',
        'rgb(102, 51, 0)',
        'rgb(153, 102, 51)',
        'rgb(187, 187, 187)',
        'rgb(136, 136, 136)',
        'rgb(68, 68, 68)',
        'rgb(0, 0, 0)'
    ]
};

export default constants;
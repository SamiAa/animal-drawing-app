import React, { useEffect, useRef } from 'react';
import loadingImage from '../images/loading-image.svg';
import constants from '../utils/constants';

function DrawingCanvas(props) {
    
    // Function to submit pixelsList into backend.
    const submitDrawing = props.submitDrawing;
    // Function to set applications state.
    // const setState = props.setState;

    // History of the canvas for undoing and redoing.
    const canvasHistory = useRef([]);

    const historyIndex = useRef(-1);

    // Create a list of pixels initialized with empty pixels.
    let pixelsList = [];

    // Set the default pen size.
    let penSize = constants.PEN_SIZE;

    // Set default color which will be used to draw.
    let drawingColor = constants.COLORS.COLOR_BLACK;

    // Current drawing color as int array.
    let drawingColorAsIntArray = [];

    // Row elements that contains the pixel elements in DrawingArea.
    const rowElementsList = useRef();

    // Mouse state and its toggling functions.
    let mouseDown = false;
    // Mouse toggling functions.
    document.body.onmousedown = function() {
        mouseDown = true;
    };
    let isModified = false;
    document.body.onmouseup = function() {
        mouseDown = false;

        // Check if the current history pixelsList is different than the pixelsList.
        // let breakLoop = false;
        // for (let i = 0; i < canvasHistory.current[historyIndex.current]['pixelsList'].length; i++) {
        //     for (let j = 0; j < canvasHistory.current[historyIndex.current]['pixelsList'][i].length; j++) {
        //         for (let k = 0; k < canvasHistory.current[historyIndex.current]['pixelsList'][i][j].length; k++) {
        //             if (canvasHistory.current[historyIndex.current]['pixelsList'][i][j][k] !== pixelsList[i][j][k]) {
        //                 breakLoop = true;
        //                 break;
        //             }
        //         }

        //         if (breakLoop) break;
        //     }

        //     if (breakLoop) break;
        // }

        // Pixels lists were not the same, so we should save.
        // if (breakLoop) {
        if (isModified) {
            setTimeout(() => saveCanvasToHistory(), 500);
            isModified = false;
        }
        // }
    };
    // Touch screen toggling functions.
    // document.body.addEventListener('touchstart', function() {
    //     mouseDown = true;
    // }, false);
    // document.body.addEventListener('touchend', function() {
    //     mouseDown = false;
    // }, false);
    let keyHistory = [];
    document.addEventListener('keydown', (event) => {
        // console.log(event.key);
        if (keyHistory.length === 0) {
            if (event.key === 'Control') {
                keyHistory.push(event.key);
            }
        } else if (keyHistory.length >= 1) {
            if (event.key === 'z' || event.key === 'y') {
                keyHistory.push(event.key);
            }

            if (keyHistory[0] === 'Control' && keyHistory[1] === 'z') {
                undo();
            } else if (keyHistory[0] === 'Control' && keyHistory[1] === 'y') {
                redo();
            }

            keyHistory = [];
        } else {
            keyHistory = [];
        }
    });

    // Parse rgb color string into int array.
    const parseColorToIntArray = (color) => {
        let rgbValues = color.split('(')[1].split(')')[0].split(', ');
        const r = parseInt(rgbValues[0]);
        const g = parseInt(rgbValues[1]);
        const b = parseInt(rgbValues[2]);
        rgbValues = [r, g, b];

        return rgbValues;
    };

    // Function to change pixelsLists values and pixel elements colors.
    // Paints 3x3 pixels in one stroke black.
    const draw = (row, pixel) => {
        if (mouseDown === true) {
            // Paint three pixel elements in the clicked row black.
            pixelsList[row][pixel] = drawingColorAsIntArray;
            rowElementsList.current[row].children[pixel].style.backgroundColor = drawingColor;

            for (let i = 1; i <= penSize; i++) {
                if (pixel-i >= 0) {
                    pixelsList[row][pixel-i] = drawingColorAsIntArray;
                    rowElementsList.current[row].children[pixel-i].style.backgroundColor = drawingColor;
                }
                if (pixel+i < constants.PIXEL_COUNT_X) {
                    pixelsList[row][pixel+i] = drawingColorAsIntArray;
                    rowElementsList.current[row].children[pixel+i].style.backgroundColor = drawingColor;
                }
                // Pain three pixel elements in the row above the clicked row black.
                if (row-i >= 0) {
                    pixelsList[row-i][pixel] = drawingColorAsIntArray;
                    rowElementsList.current[row-i].children[pixel].style.backgroundColor = drawingColor;
                    if (pixel-i >= 0) {
                        pixelsList[row-i][pixel-i] = drawingColorAsIntArray;
                        rowElementsList.current[row-i].children[pixel-i].style.backgroundColor = drawingColor;
                    }
                    if (pixel+i < constants.PIXEL_COUNT_X) {
                        pixelsList[row-i][pixel+i] = drawingColorAsIntArray;
                        rowElementsList.current[row-i].children[pixel+i].style.backgroundColor = drawingColor;
                    }
                }
    
                // Paint three pixel elements in the row below the clicked row black.
                if (row+i < constants.PIXEL_COUNT_Y) {
                    pixelsList[row+i][pixel] = drawingColorAsIntArray;
                    rowElementsList.current[row+i].children[pixel].style.backgroundColor = drawingColor;
                    if (pixel-i >= 0) {
                        pixelsList[row+i][pixel-i] = drawingColorAsIntArray;
                        rowElementsList.current[row+i].children[pixel-i].style.backgroundColor = drawingColor;
                    }
                    if (pixel+i < constants.PIXEL_COUNT_X) {
                        pixelsList[row+i][pixel+i] = drawingColorAsIntArray;
                        rowElementsList.current[row+i].children[pixel+i].style.backgroundColor = drawingColor;
                    }
                }
            }

            isModified = true;
        }
    };

    const fill = (row, pixel) => {
        // const color = rowElementsList[row].children[pixel].style.backgroundColor;
        const rgbValues = pixelsList[row][pixel];

        // pixelsList[row][pixel] = drawingColorAsIntArray;
        // rowElementsList.current[row].children[pixel].style.backgroundColor = drawingColor;

        for (let i = row; i >= 0; i--) {
            if (pixelsList[i][pixel] !== rgbValues) break;

            for (let j = pixel; j >= 0; j--) {
                if (pixelsList[i][j] === rgbValues) {
                    pixelsList[i][j] = drawingColorAsIntArray;
                    rowElementsList.current[i].children[j].style.backgroundColor = drawingColor;
                } else {
                    break;
                }
            }
        }

        for (let i = row; i >= 0; i--) {
            if (pixelsList[i][pixel+1] !== rgbValues) break;

            for (let j = pixel+1; j < constants.PIXEL_COUNT_X; j++) {
                if (pixelsList[i][j] === rgbValues) {
                    pixelsList[i][j] = drawingColorAsIntArray;
                    rowElementsList.current[i].children[j].style.backgroundColor = drawingColor;
                } else {
                    break;
                }
            }
        }

        for (let i = row+1; i < constants.PIXEL_COUNT_Y; i++) {
            if (pixelsList[i][pixel] !== rgbValues) break;
            
            for (let j = pixel; j < constants.PIXEL_COUNT_X; j++) {
                if (pixelsList[i][j] === rgbValues) {
                    pixelsList[i][j] = drawingColorAsIntArray;
                    rowElementsList.current[i].children[j].style.backgroundColor = drawingColor;
                } else {
                    break;
                }
            }
        }

        for (let i = row+1; i < constants.PIXEL_COUNT_Y; i++) {
            if (pixelsList[i][pixel-1] !== rgbValues) break;

            for (let j = pixel-1; j >= 0; j--) {
                if (pixelsList[i][j] === rgbValues) {
                    pixelsList[i][j] = drawingColorAsIntArray;
                    rowElementsList.current[i].children[j].style.backgroundColor = drawingColor;
                } else {
                    break;
                }
            }
        }

        isModified = true;
    };

    // const touchMove = (event) => {
    //     const row = event.target.getAttribute('row');
    //     const pixel = event.target.getAttribute('pixel');
    //     draw(row, pixel);
    //     // event.preventDefault();
    // };

    const submit = () => {
        document.querySelector('.DrawingCanvas-dialog').setAttribute('style', 'display: flex');
        document.querySelector('.DrawingCanvas-dialog .Message').setAttribute('style', 'display: none');
        document.querySelector('.LoadingImage').setAttribute('style', 'display: block');

        new Promise(async (resolve, reject) => {
            const response = await submitDrawing(pixelsList);

            if (response.status === 200 && response.data !== 'Got nothing!') {
                let splitData = response.data.split(';');
                let str = "";
                for (let i = 0; i < splitData.length; i++) {
                    if (i == 0) str += `<h2>${splitData[i]}</h2>`;
                    else str += `<div>${splitData[i]}</div>`;
                }
                resolve(str);
            } else {
                reject();
            }
        })
        .then(
            (data) => {
                const message = document.querySelector('.DrawingCanvas-dialog .Message');
                message.setAttribute('style', 'display: flex');
                message.innerHTML = data;
                message.setAttribute('style', 'color: black');
                document.querySelector('.LoadingImage').setAttribute('style', 'display: none');
            },
            () => {
                const message = document.querySelector('.DrawingCanvas-dialog .Message');
                message.setAttribute('style', 'display: flex');
                message.innerHTML = 'Something went wrong!';
                message.setAttribute('style', 'color: red');
                document.querySelector('.LoadingImage').setAttribute('style', 'display: none');
            }
        );
    };

    const initPixelsList = () => {
        let rgbValues = parseColorToIntArray(constants.COLORS.COLOR_WHITE);

        pixelsList = [];

        for (let y = 0; y < constants.PIXEL_COUNT_Y; y++) {
            let row = [];
            for (let x = 0; x < constants.PIXEL_COUNT_X; x++) {
                row.push(rgbValues);
            }

            pixelsList.push(row);
        }
    };

    initPixelsList();

    // const initRowElementsList = () => {
    //     for (let i = 0; i < rowElementsList.current.length; i++) {
    //         for (let j = 0; j < rowElementsList.current[i].children.length; j++) {
    //             rowElementsList.current[i].children[j].addEventListener('touchmove', touchMove, { passive: false })
    //         }
    //     }
    // };

    const clearDrawing = () => {
        initPixelsList();

        // rowElementsList.current = document.querySelectorAll('.Row');

        for (let i = 0; i < rowElementsList.current.length; i++) {
            for (let j = 0; j < rowElementsList.current[i].children.length; j++)
            rowElementsList.current[i].children[j].style.backgroundColor = constants.COLORS.COLOR_WHITE;
        }

        // saveCanvasToHistory();
        isModified = true;
    };

    const saveCanvasToHistory = () => {
        // if (canvasHistory.current.length < 100) {
        //     historyIndex.current = historyIndex.current + 1;
        // } else {
        //     canvasHistory.current.splice(0, 1);
        // }
        historyIndex.current = historyIndex.current + 1;

        let pixelsListCopy = [];
        for (let i = 0; i < pixelsList.length; i++) {
            let row = [];
            for (let j = 0; j < pixelsList[i].length; j++) {
                // let pixelRGB = [];
                // for (let k = 0; k < pixelsList[i][j].length; k++) {
                //     pixelRGB.push(pixelsList[i][j][k]);
                // }

                // row.push(pixelRGB);
                row.push(pixelsList[i][j]);
            }

            pixelsListCopy.push(row);
        }

        let rowElementsColorsList = [];
        for (let i = 0; i < rowElementsList.current.length; i++) {
            let row = [];
            for (let j = 0; j < rowElementsList.current[i].children.length; j++) {
                row.push(rowElementsList.current[i].children[j].style.backgroundColor);
            }
            
            rowElementsColorsList.push(row);
        }

        // If historyIndex.current points to the last index of canvasHistory array.
        if (historyIndex.current-1 < canvasHistory.current.length-1) {
            let amountToRemove = (canvasHistory.current.length) - (historyIndex.current);
            canvasHistory.current.splice(historyIndex.current, amountToRemove);
            // for (let i = historyIndex.current-1; i < canvasHistory.current.length; i++) {
            //     canvasHistory.current.splice(i, 1);
            // }
        }

        canvasHistory.current.push({
            pixelsList: pixelsListCopy,
            rowElementsColorsList: rowElementsColorsList
        });

        // console.log("index" + historyIndex.current);
        // console.log("length" + canvasHistory.current.length);
    };

    const undo = () => {
        if (historyIndex.current > 0) {
            historyIndex.current = historyIndex.current - 1;
            pixelsList = canvasHistory.current[historyIndex.current]['pixelsList'];

            for (let i = 0; i < rowElementsList.current.length; i++) {
                for (let j = 0; j < rowElementsList.current[i].children.length; j++) {
                    rowElementsList.current[i].children[j].style.backgroundColor = canvasHistory.current[historyIndex.current]['rowElementsColorsList'][i][j];
                }
            }

            if (historyIndex.current <= 0) {
                clearDrawing();
            }
        }

        // console.log(historyIndex.current);
    };

    const redo = () => {
        if (historyIndex.current < canvasHistory.current.length-1) {
            historyIndex.current = historyIndex.current + 1;
            pixelsList = canvasHistory.current[historyIndex.current]['pixelsList'];

            for (let i = 0; i < rowElementsList.current.length; i++) {
                for (let j = 0; j < rowElementsList.current[i].children.length; j++) {
                    rowElementsList.current[i].children[j].style.backgroundColor = canvasHistory.current[historyIndex.current]['rowElementsColorsList'][i][j];
                }
            }
        }

        // console.log(historyIndex.current);
    };

    const hideMessage = () => {
        document.querySelector('.DrawingCanvas-dialog').setAttribute('style', 'display: none');
    };

    // Get the row elements once when the app has renderd for the first time.
    useEffect(() => {
        rowElementsList.current = document.querySelectorAll('.Row');
        drawingColorAsIntArray = parseColorToIntArray(constants.COLORS.COLOR_BLACK);
        saveCanvasToHistory();
        clearDrawing();
        // setState(1);
        // initRowElementsList();
    }, []);

    return(
        <div className="DrawingCanvas">
            <div className="DrawingCanvas-wrapper">
                <h1>Drawing Game</h1>
                <h3>Draw elephant, cow, butterfly, sheep or squirrel...</h3>
                <div
                    className="DrawArea"
                    style={ {
                        minHeight: constants.DRAW_AREA_HEIGHT + constants.MEASURE_TYPE,
                        minWidth: constants.DRAW_AREA_WIDTH + constants.MEASURE_TYPE
                     } }>
                        {pixelsList.map((row, i) =>
                            <div
                                className="Row"
                                style={ {
                                    minHeight: (constants.DRAW_AREA_HEIGHT / constants.PIXEL_COUNT_Y) + constants.MEASURE_TYPE
                                } }
                                key={i}
                            >
                                {row.map((pixel, j) =>
                                    <button
                                        type="button"
                                        className="Pixel"
                                        style={ {
                                            backgroundColor: constants.COLORS.COLOR_WHITE,
                                            minHeight: (constants.DRAW_AREA_HEIGHT / constants.PIXEL_COUNT_Y) + constants.MEASURE_TYPE,
                                            minWidth: (constants.DRAW_AREA_WIDTH / constants.PIXEL_COUNT_X) + constants.MEASURE_TYPE
                                        } }
                                        onMouseLeave={() => draw(i, j)}
                                        // onTouchMove={touchMove}
                                        // row={i}
                                        // pixel={j}
                                        onDoubleClick={() => fill(i, j)}
                                        key={j}>
                                    </button>)}
                            </div>)}
                </div>
                <div
                    className="Toolbar"
                    // style={ { minWidth: constants.DRAW_AREA_WIDTH + constants.MEASURE_TYPE } }
                >
                    {constants.COLORS_ARRAY.map((color, i) =>
                        <button
                            className="Color"
                            style={ {
                                // minWidth: (2 * (100/constants.COLORS_ARRAY.length)) + '%',
                                minHeight: (100/constants.COLORS_ARRAY.length) + '%',
                                backgroundColor: color
                            } }
                            onClick={() => {
                                drawingColor = color;
                                drawingColorAsIntArray = parseColorToIntArray(color);
                            }}
                            key={i}>
                                <div className="Color-marker"></div>
                        </button>)
                    }
                </div>
                <div className="Toolbar-2">
                    <button
                        className="Pen"
                        onClick={() => penSize = 1}
                    >
                        L
                        <div className="Pen-marker"></div>
                    </button>
                    <button
                        className="Pen"
                        onClick={() => penSize = 0}
                    >
                        S
                        <div className="Pen-marker"></div>
                    </button>
                </div>
                <div
                    className="Buttons-wrapper"
                    style={ { width: constants.DRAW_AREA_WIDTH + constants.MEASURE_TYPE } }
                >
                    <button
                        className="SubmitButton DialogButton"
                        onClick={() => submit()}
                    >
                            Submit
                    </button>
                    <button
                        className="CancelButton DialogButton"
                        onClick={() => clearDrawing()}
                    >
                        Clear
                    </button>
                </div>
            </div>
            <div className="DrawingCanvas-dialog">
                <h3>Prediction</h3>
                <div className="Message-wrapper">
                    <div className="Message"></div>
                    <img className="LoadingImage" src={loadingImage} alt="Loading image" />
                </div>
                <div className="Buttons-wrapper">
                    <button
                        className="SubmitButton DialogButton"
                        onClick={() => hideMessage()}>
                            OK
                        </button>
                    {/* <button className="DialogButton">No</button> */}
                </div>
            </div>
        </div>
    );
}

export default DrawingCanvas;